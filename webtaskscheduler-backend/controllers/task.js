const cron = require('node-cron');
const fetch = require('node-fetch');


//req solicitud
//res response
const executeTask = async(req, res) => {
    
    const {pvCron, pvUrl, pvNumCaracteres } = req.body;
    
    try{
        cron.schedule(pvCron, async function(){
            console.log('-----------------');
            console.log('running crob job');
            const response = await fetch(pvUrl);
            const data = await response.text();
            
            if (response.ok) {
                const final = data.substring(0,pvNumCaracteres);
                console.log(final);
                res.status(200).json({
                    ok: true,
                    message: "the task was created succesfully",
                })
            }else{
                console.log('error reading the website');
            }
        })  

     }catch(error){
         res.status(500).json({
             ok: false,
             msg:'There was an error; please try again'
         });
    }
}


;
module.exports = {
    executeTask
}