const express = require('express');
require('dotenv').config();
const cors = require('cors');
console.log(process.env);

//crear el servidor de express
const app = express();

//base de datos
//dbConnection();

//CORS
app.use(cors());
//directorio publico
app.use( express.static('public')) ;

//lectura y parseo del body
app.use( express.json() );

//Rutas
// auth // create, login, renew
app.use('/api/tasks',require('./routes/tasks')) ;

//app.use('/api/events',require('./routes/events')) ;

// to crud:eventos


//app.get('/', (req,res)=> {
//    Response.json({
//        ok:true
//    })
//});

//escuchar peticiones
app.listen( process.env.PORT , () => {
    console.log(`Servidor corriendo en puerto ${process.env.PORT }`);
});
//console.log('hola mundo');