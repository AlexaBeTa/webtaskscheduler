const express = require('express');
const { Router } = require('express');
const { executeTask } = require('../controllers/task');
const router = express.Router();
/*
   Routes
    /api/tasks

*/

router.post('/', executeTask);



module.exports = router;
