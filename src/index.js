import React from 'react';
import ReactDOM from 'react-dom';
import { AppTaskscheduler } from './AppTaskscheduler';
import './styles.css';

ReactDOM.render(
  <AppTaskscheduler/>,
  document.getElementById('root')
);