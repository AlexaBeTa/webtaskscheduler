import React from 'react'

import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";

import { Home } from '../components/home'


export const AppRouter = () => {

  return (
    <BrowserRouter>
      <div>
        <Routes>
            <Route path={"/"} element={<Home />}></Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
    
  }