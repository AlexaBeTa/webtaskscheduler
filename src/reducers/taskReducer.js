import { types } from '../types/types';

const initialState = {
     message:null,
     cron:null,
     url:null,
     numcaracteres:0
}

export const taskReducer = ( state = initialState, action ) => {

    switch ( action.type ) {
        
        case types.makeSchedule:
            return {
                ...state,
                ...action.payload,
            }

        case types.resetFormSchedule:
            return {
                ...action.payload,
                state: initialState
            }
        default:
            return state;
    }

}


