import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk'

import { taskReducer } from '../reducers/taskReducer';


const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

export const store = createStore(
    taskReducer,
    composeEnhancers(
        applyMiddleware( thunk )
    )
);
