const baseUrl = process.env.REACT_APP_API_URL;

export const fetchCall = (endpoint, data, method = 'GET') => {
    const url =`${ baseUrl }/${ endpoint }` ;//LOCALHOST:4000/API/TASKS
    console.log(url);
    if( method === 'GET'){
        return fetch( url );
    }else {
        return fetch( url, {
            method,
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify( data )
        });
    }
}