import { fetchCall } from '../helpers/fetchCall';
import { types } from '../types/types';
import Swal from 'sweetalert2';

const created = ( task ) => ({
    type: types.makeSchedule,
    payload: task
});

export const Reset = () => ({ type: types.resetFormSchedule })

export const ApiFunction = ( pvCron,pvUrl,pvNumCaracteres ) => {

    return async(dispatch) => {
        try {
            const resp = await fetchCall( 'tasks', { pvCron, pvUrl, pvNumCaracteres}, 'POST');
            const body = await resp.json();
            console.log(resp);
            console.log(body);

            if (body.ok){        
                dispatch( created ({
                    message:body.message,
                    cron:body.cron,
                    url:body.url,
                    numcaracteres:body.numcaracteres
                }))
                        
            }else{
                Swal.fire('Error',body.msg, 'error');
            }   
        } catch (error) {
            Swal.fire('Error',error, 'error');
        }
        
    }
}
