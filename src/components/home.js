import React from 'react';
import { useState } from 'react';
import { useDispatch ,useSelector} from 'react-redux';
import { ApiFunction,Reset } from '../helpers/apiFunction';
import Swal from 'sweetalert2';


export const Home = () => {
 
  const { message } = useSelector( state => state || {} );

  const dispatch = useDispatch();

  const [formData, setFormData] = useState({
    lCron: "0 0 15 * * *",
    lUrl: "https://www.eltiempo.com",
    lnumCaracteres: 1000
  });

  const {  lCron,lUrl,lnumCaracteres } = formData;
  
  const handleInputChange = (e) => {
      setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = ( e ) =>{
    dispatch( Reset() );
    e.preventDefault();
    let error =false;
    let mensajeError = '';
    
    console.dir(formData);
    console.log(formData);
    if (lUrl === ''){
        mensajeError += 'La url es necesaria ';
        error = true;
    }
    
    if (lCron === ''){
        mensajeError +=  'La expresion CORN es necesaria ';
        error = true;
    }
    //console.log(errorObj);
    if (error) 
    {
        return Swal.fire('error', mensajeError, 'error');
    }

    dispatch( ApiFunction(lCron,lUrl,lnumCaracteres)  );
    
}


  
  return(
    <>
    <div className="col-md-8 offset-md-2">
      
      <h2>Web scheduler </h2>  
            <div className="form-group">
              <label htmlFor="InputCron">CRON:</label>
              <input 
                  type="text"
                  className="form-control"
                  placeholder="CRON"
                  name="lCron" 
                  aria-describedby="cronHelp"
                  id="InputCron"
                  onChange={handleInputChange}
                  value={lCron}
              />
              <small id="cronHelp" className="form-text text-muted">Cron is an expresion to execute tasks repeatively.</small>
                        
            </div>
            <div className="form-group">
              <label htmlFor="InputUrl">URL:</label>
              <input 
                    type="text"
                    className="form-control"
                    placeholder="URL"
                    name="lUrl" 
                    aria-describedby="urlHelp"
                    id="InputUrl"
                    onChange={handleInputChange}
                    value={lUrl}
                />
                <small id="urlHelp" className="form-text text-muted">the sintax of url goes https://www... </small>
                
            </div>

            <div className="form-group">
              <label htmlFor="InputNumCaracteres">Cantidad de caracteres:</label>
              <input 
                    type="text"
                    className="form-control"
                    placeholder="Numero de carácteres"
                    name="lnumCaracteres" 
                    aria-describedby="lnumCaracteresHelp"
                    id="InputNumCaracteres"
                    onChange={handleInputChange}
                    value={lnumCaracteres}
                />
                <small id="urlHelp" className="form-text text-muted"></small>    
            </div>

            
            <button type="submit" onClick={handleSubmit} className="btn btn-primary">Crear tarea</button>
          
        </div>



        {
          message && <div className='col-md-8 offset-md-2'>
            <h2>{message}</h2>
          </div>
        }
        </>
  )
  ;
};
