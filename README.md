	The folder that contains the frontend is WebTaskScheduler, it is necessary to run the command "npm install" to get all the libraries
	and after that, run the command "npm start"
	
	The folder that contains the backend is in webtaskscheduler folder, and is called "webtaskscheduler-backend", it is necessary to run 
	the command "npm install" to get all the libraries and after that run the command "npm run dev" to view the execution of the
	cronjob on the console
	
	the cronjob is to get the first "n" characters from the website from the URL that we write in the input 
	field
	
	Parameters in the view
	Cron: expression, specifies when it should execute the task 
	URL: https:www.exampleofwebsite.com
	Cantidad de caracteres: an integer, bigger to 0